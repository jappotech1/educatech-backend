import {
  Resolver,
  Query,
  Mutation,
  Args,
  ResolveField,
  Parent,
} from '@nestjs/graphql';
import { UtilisateursService } from './utilisateurs.service';
import { Utilisateur } from './entity/utilisateur.entity';
import {
  FindManyUtilisateurArgs,
  FindUniqueUtilisateurArgs,
} from './dtos/find.args';
import { CreateUtilisateurInput } from './dtos/create-utilisateur.input';
import { UpdateUtilisateurInput } from './dtos/update-utilisateur.input';
import {
  checkRowLevelPermission,
  checkUserAffiliation,
} from 'src/common/auth/util';
import { GetUserType } from 'src/common/types';
import { AllowAuthenticated, GetUser } from 'src/common/auth/auth.decorator';
import { PrismaService } from 'src/common/prisma/prisma.service';
import { Prisma } from '@prisma/client';
import { Res } from '@nestjs/common';
import { Etablissement } from '../etablissements/entity/etablissement.entity';
import { Adresse } from '../adresses/entity/adresse.entity';
import { Contact } from '../contacts/entity/contact.entity';

@Resolver(() => Utilisateur)
export class UtilisateursResolver {
  constructor(
    private readonly utilisateursService: UtilisateursService,
    private readonly prisma: PrismaService,
  ) { }

  // @AllowAuthenticated()
  @Mutation(() => Utilisateur)
  createUtilisateur(
    @Args('createUtilisateurInput') args: CreateUtilisateurInput,
    @GetUser() user: GetUserType,
  ) {
    // // // checkRowLevelPermission(user, args.uid)
    return this.utilisateursService.create(args);
  }

  @AllowAuthenticated()
  @Query(() => [Utilisateur], { name: 'utilisateurs' })
  async findAll(
    @Args() args: FindManyUtilisateurArgs,
    @GetUser() user: GetUserType,
  ) {
    const affiliation = await checkUserAffiliation(user);
    if (affiliation) {
      return this.utilisateursService.findAllByEtablissement(
        args,
        affiliation.etablissementId,
      );
    }
    return this.utilisateursService.findAll(args);
  }

  @AllowAuthenticated()
  @Query(() => Utilisateur, { name: 'utilisateur' })
  findOne(@Args() args: FindUniqueUtilisateurArgs) {
    return this.utilisateursService.findOne(args);
  }

  @AllowAuthenticated()
  @Query(() => Utilisateur, { name: 'me' })
  findMe(@GetUser() user: GetUserType) {
    console.log('🚀 ~ UtilisateursResolver ~ findMe ~ user:', user);
    // checkRowLevelPermission(user, args.uid)
    return this.utilisateursService.findMe(user.uid);
  }

  @AllowAuthenticated()
  @Mutation(() => Utilisateur)
  async updateUtilisateur(
    @Args('updateUtilisateurInput') args: UpdateUtilisateurInput,
    @GetUser() user: GetUserType,
  ) {
    const utilisateur = await this.prisma.utilisateur.findUnique({
      where: { id: args.id },
    });
    // // checkRowLevelPermission(user, utilisateur.uid)
    return this.utilisateursService.update(args);
  }

  @AllowAuthenticated()
  @Mutation(() => Utilisateur)
  async removeUtilisateur(
    @Args() args: FindUniqueUtilisateurArgs,
    @GetUser() user: GetUserType,
  ) {
    const utilisateur = await this.prisma.utilisateur.findUnique(args);
    // // checkRowLevelPermission(user, utilisateur.uid)
    return this.utilisateursService.remove(args);
  }

  @ResolveField(() => Etablissement || null)
  async etablissement(@Parent() parent: Utilisateur) {
    if (!parent.etablissementId || parent.etablissementId === null) {
      return {};
    }
    return this.prisma.etablissement.findUnique({
      where: { id: parent.etablissementId },
    });
  }

  @ResolveField(() => Adresse || null)
  async adresse(@Parent() parent: Utilisateur) {
    if (!parent.adresseId || parent.adresseId === null) {
      return {};
    }
    return this.prisma.adresse.findUnique({
      where: { id: parent.adresseId },
    });
  }

  @ResolveField(() => Contact || null)
  async contact(@Parent() parent: Utilisateur) {
    if (!parent.contactId || parent.contactId === null) {
      return {};
    }
    return this.prisma.contact.findUnique({
      where: { id: parent.contactId },
    });
  }
}
