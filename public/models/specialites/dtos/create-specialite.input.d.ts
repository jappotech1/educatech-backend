import { Specialite } from '../entity/specialite.entity';
declare const CreateSpecialiteInput_base: import("@nestjs/common").Type<Pick<Specialite, "nom" | "etablissementId" | "mentionId">>;
export declare class CreateSpecialiteInput extends CreateSpecialiteInput_base {
}
export {};
