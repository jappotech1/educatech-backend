import { AnneeScolaire } from '../entity/annee-scolaire.entity';
declare const CreateAnneeScolaireInput_base: import("@nestjs/common").Type<Pick<AnneeScolaire, "nom" | "dateDebut" | "dateFin">>;
export declare class CreateAnneeScolaireInput extends CreateAnneeScolaireInput_base {
}
export {};
