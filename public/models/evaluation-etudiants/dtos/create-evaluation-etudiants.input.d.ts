import { EvaluationEtudiants } from '../entity/evaluation-etudiants.entity';
declare const CreateEvaluationEtudiantsInput_base: import("@nestjs/common").Type<Pick<EvaluationEtudiants, "coursId" | "description" | "duree" | "dateEvaluation" | "typeEvaluation" | "document">>;
export declare class CreateEvaluationEtudiantsInput extends CreateEvaluationEtudiantsInput_base {
}
export {};
