import { ReseauSocial } from '../entity/reseau-social.entity';
declare const CreateReseauSocialInput_base: import("@nestjs/common").Type<Pick<ReseauSocial, "nom" | "contactId" | "username" | "url">>;
export declare class CreateReseauSocialInput extends CreateReseauSocialInput_base {
}
export {};
