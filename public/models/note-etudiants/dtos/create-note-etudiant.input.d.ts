import { NoteEtudiant } from '../entity/note-etudiant.entity';
declare const CreateNoteEtudiantInput_base: import("@nestjs/common").Type<Pick<NoteEtudiant, "etudiantId" | "note" | "evaluationEtudiantId">>;
export declare class CreateNoteEtudiantInput extends CreateNoteEtudiantInput_base {
}
export {};
