import { UniteEnseignement } from '../entity/unite-enseignement.entity';
declare const CreateUniteEnseignementInput_base: import("@nestjs/common").Type<Pick<UniteEnseignement, "nom" | "code" | "credit" | "numero" | "semestreId">>;
export declare class CreateUniteEnseignementInput extends CreateUniteEnseignementInput_base {
}
export {};
