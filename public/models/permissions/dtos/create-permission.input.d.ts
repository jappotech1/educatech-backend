import { Permission } from '../entity/permission.entity';
declare const CreatePermissionInput_base: import("@nestjs/common").Type<Pick<Permission, "role" | "label">>;
export declare class CreatePermissionInput extends CreatePermissionInput_base {
}
export {};
